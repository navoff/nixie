/**
 * @file pwm.h
 * @brief
 * @author Постнов В.М.
 * @date 8 мар. 2017 г.
 * @addtogroup
 * @{
*/

#ifndef PWM_H_
#define PWM_H_

// ======================================= Definition ==============================================

/// Максимально значение счётчика ШИМ
#define PWM_CNT_MAX                   1000

/// Минимальное значение тактов счётчика, на которое светодиод включается
#define PWM_ON_MIN                    100
/// Максимально значение тактов счётчика, на которое светодиод включается
#define PWM_ON_MAX                    900

/// Количество повторений "прохода" счётчика, прежде чем скважность будет изменена
#define PWM_REPETITION                1

#define PWM_FAST_CNT_THRESHOLD        600

typedef struct
{
  uint8_t on_req;
  uint8_t start_pulse_req;
  uint8_t off_req;
  enum
  {
    /// Ожидание команды
    PWM_ST__WAIT,
    /// Выполняется плавное зажигание светодиода
    PWM_ST__ON,
    /// Выполняется плавное гашение светодиода
    PWM_ST__OFF,
  } state;
  int16_t cmp;
  int16_t repetition_cnt;

  /// Число изменения скважности в диапазоне тусклого свечения
  int8_t dull_inc;

  /// Число изменения скважности в диапазоне яркого свечения
  int8_t bright_inc;

  void (*led_on)(void);
  void (*led_off)(void);
  void (*set_hw_ccr)(uint16_t cmp);
} pwm_t;

// ======================================= Declaration =============================================

void pwm_init(
    pwm_t *pwm,
    void (*led_on)(void),
    void (*led_off)(void),
    void (*pwm_set_ccr)(uint16_t cmp));
void pwm_process(pwm_t *pwm);

#endif /* PWM_H_ */

/** @} */
