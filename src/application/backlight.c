/**
 * @file backlight.c
 * @brief
 * @author Постнов В.М.
 * @date 12 мар. 2017 г.
 * @addtogroup
 * @{
*/

#include "config.h"
#include "backleds.h"
#include "tick_timer.h"
#include "nvram.h"

#include "backlight.h"

// ======================================= Definition ==============================================

#define BACKLIGHT_EFFECT_CHANGE_PAUSE     100

#pragma pack(push, 1)
typedef struct
{
  uint8_t msk;
  uint16_t duration;
} backlight_effect_state_t;

typedef struct
{
  uint8_t len;
  int8_t pwm_dull_inc;
  int8_t pwm_bright_inc;
  const backlight_effect_state_t state[];
} backlight_effect_t;

typedef struct
{
  enum
  {
    BACKLIGHT_ST__PAUSE,
    BACKLIGHT_ST__PLAY_EFFECT,
  } state;
  uint8_t next_effect_req;
  uint8_t new_effect_req;
  uint8_t new_effect_idx;
  uint32_t timestamp;
  uint8_t effect_state_idx;
  uint16_t effect_state_duration;
  uint8_t effects_amount;
  uint8_t current_effect_idx;
  const backlight_effect_t *current_effect;
  const backlight_effect_t *effects[];
} backlight_t;
#pragma pack(pop)

// ======================================= Declaration =============================================

void backlight_apply_current_effect(void);

const backlight_effect_t backlight_effect__max_min = {
    .len = 1,
    .pwm_dull_inc = 1,
    .pwm_bright_inc = 3,
    .state = {
        { .msk = 0x0F, .duration = 15000, },
    }
};

const backlight_effect_t backlight_effect__run = {
    .len = 5,
    .pwm_dull_inc = 10,
    .pwm_bright_inc = 20,
    .state = {
        { .msk = 0x01, .duration = 250, },
        { .msk = 0x02, .duration = 250, },
        { .msk = 0x04, .duration = 250, },
        { .msk = 0x08, .duration = 250, },
        { .msk = 0x00, .duration = 2000, },
    }
};

const backlight_effect_t backlight_effect__reverse = {
    .len = 10,
    .pwm_dull_inc = 10,
    .pwm_bright_inc = 20,
    .state = {
        { .msk = 0x08, .duration = 250, },
        { .msk = 0x04, .duration = 250, },
        { .msk = 0x02, .duration = 250, },
        { .msk = 0x01, .duration = 250, },
        { .msk = 0x00, .duration = 1000, },
        { .msk = 0x01, .duration = 250, },
        { .msk = 0x02, .duration = 250, },
        { .msk = 0x04, .duration = 250, },
        { .msk = 0x08, .duration = 250, },
        { .msk = 0x00, .duration = 1000, },
    }
};

const backlight_effect_t backlight_effect__off = {
    .len = 0,
};

const backlight_effect_t backlight_effect__min = {
    .len = 1,
    .pwm_dull_inc = 1,
    .pwm_bright_inc = 1,
    .state = {
        { .msk = 0x00, .duration = 5000, },
    }
};

backlight_t backlight = {
    .effects_amount = 5,
    .effects =
    {
        &backlight_effect__off,
        &backlight_effect__reverse,
        &backlight_effect__run,
        &backlight_effect__max_min,
        &backlight_effect__min,
    },
};

// ======================================== Implementation =========================================

void backlight_init(void)
{
  NVRAM_SETTINGS_READ_FIELD(backlight_effect_idx, &backlight.current_effect_idx);
  if (backlight.current_effect_idx >= backlight.effects_amount)
  {
    backlight.current_effect_idx = BACKLIGHT_EFFECT_IDX__DEFAULT;
  }

  backlight.next_effect_req = 0;
  backlight.new_effect_req = 0;
  backlight.state = BACKLIGHT_ST__PLAY_EFFECT;
  backlight_apply_current_effect();
}

void backlight_process(void)
{
  switch (backlight.state)
  {
    case BACKLIGHT_ST__PAUSE:
      if (CHECK_TIMER(backlight.timestamp, BACKLIGHT_EFFECT_CHANGE_PAUSE))
      {
        backlight_apply_current_effect();
        
        backlight.state = BACKLIGHT_ST__PLAY_EFFECT;
      }
    break;

    case BACKLIGHT_ST__PLAY_EFFECT:
      // если требуется перейти к следующему эффекту
      if (backlight.next_effect_req)
      {
        backlight.next_effect_req = 0;

        backleds_off(0xFF);

        backlight.current_effect_idx++;
        if (backlight.current_effect_idx >= backlight.effects_amount)
        {
          backlight.current_effect_idx = 0;
        }
        NVRAM_SETTINGS_WRITE_FIELD(backlight_effect_idx, &backlight.current_effect_idx);

        START_TIMER(backlight.timestamp);
        backlight.state = BACKLIGHT_ST__PAUSE;
        break;
      }

      if (backlight.new_effect_req)
      {
        backlight.new_effect_req = 0;

        backleds_off(0xFF);

        backlight.current_effect_idx = backlight.new_effect_idx;
        NVRAM_SETTINGS_WRITE_FIELD(backlight_effect_idx, &backlight.current_effect_idx);

        START_TIMER(backlight.timestamp);
        backlight.state = BACKLIGHT_ST__PAUSE;
        break;
      }

      if (backlight.current_effect->len != 0)
      {
        if (CHECK_TIMER(backlight.timestamp, backlight.effect_state_duration))
        {
          START_TIMER(backlight.timestamp);

          // перейти к следующему такту эффекта
          backlight.effect_state_idx++;
          if (backlight.effect_state_idx >= backlight.current_effect->len)
          {
            backlight.effect_state_idx = 0;
          }

          // включить текущий такт и настроить длительность
          backleds_start_pulse(backlight.current_effect->state[backlight.effect_state_idx].msk);
          backlight.effect_state_duration = backlight.current_effect->state[backlight.effect_state_idx].duration;
        }
      }
    break;
  }
}

void backlight_set_effect(uint8_t idx)
{
  // todo добавить проверку на корректность
  backlight.new_effect_idx = idx;
  backlight.new_effect_req = 1;
}

void backlight_set_next_effect(void)
{
  backlight.next_effect_req = 1;
}

void backlight_set_effect_off(void)
{
  backlight_set_effect(0);
}

void backlight_set_effect_reverse(void)
{
  // todo убрать магические числа
  backlight_set_effect(1);
}

void backlight_apply_current_effect(void)
{
  backlight.current_effect = backlight.effects[backlight.current_effect_idx];
  backlight.effect_state_duration = 0;
  backlight.effect_state_idx = -1;

  if (backlight.current_effect->len != 0)
  {
    backleds_set_pwm_inc(
        backlight.current_effect->pwm_dull_inc, backlight.current_effect->pwm_bright_inc);
    backleds_on(0xFF);

    START_TIMER(backlight.timestamp);
  }
}

/** @} */
