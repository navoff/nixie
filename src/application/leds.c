/**
 * @file leds.c
 * @brief
 * @author Постнов В.М.
 * @date 28 янв. 2017 г.
 * @addtogroup
 * @{
*/

#include "config.h"

#include "gpio.h"

#include "leds.h"

// ======================================= Definition ==============================================

// ======================================= Declaration =============================================

// ======================================== Implementation =========================================

void leds_init(void)
{
  gpio_rcc_cmd(LED1_GPIO_IDX, ENABLE);
  gpio_pin_set_mode(LED1_GPIO_IDX, LED1_PIN_IDX, GPIO_MODE_OUT_PP);
  gpio_pin_set_mode(LED2_GPIO_IDX, LED2_PIN_IDX, GPIO_MODE_OUT_PP);

  LED1_OFF();
  LED2_OFF();
}

/** @} */
