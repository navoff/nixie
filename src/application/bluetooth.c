/**
 * @file bluetooth.c
 * @brief
 * @author Постнов В.М.
 * @date 23 мар. 2017 г.
 * @addtogroup
 * @{
*/

#include "config.h"

#include "tick_timer.h"
#include "bluetooth_uart.h"

#include "bluetooth.h"


// ======================================= Definition ==============================================

#define BT_RESET_GPIO                       GPIOB
#define BT_RESET_PIN                        GPIO_Pin_5
#define BT_RESET_DOWN()                     BT_RESET_GPIO->BSRR = BT_RESET_PIN
#define BT_RESET_UP()                       BT_RESET_GPIO->BRR  = BT_RESET_PIN

#define BT_RX_PROCESS_BUF_SIZE              128

#define BT_EVENT__RX_OK                     0x00000001
#define BT_EVENT__RX_SET_NAME               0x00000002
#define BT_EVENT__RX_SET_PIN                0x00000004
#define BT_EVENT__TIMEOUT                   0x80000000

#define TO_UP_CASE(x)         (((x >= 'a') && (x <= 'z')) ? x - ('a' - 'A') : x)

typedef struct
{
  const char *str;
  const uint32_t msk;
} bt_rx_event_t;

typedef struct
{
  const char *str;
  const uint32_t timeout;
  const uint32_t rx_ok_msk;
} bt_script_t;

typedef struct
{
  enum
  {
    BT_ST__WAIT_BEFORE_START,
    BT_ST__START,
    BT_ST__RESET_DELAY,
    BT_ST__PRE_RUNNING,
    BT_ST__RUNNING,
    BT_ST__WAIT_SCRIPT_OK,
  } state;

  uint8_t rx_process_buf[BT_RX_PROCESS_BUF_SIZE];
  uint16_t rx_process_buf_idx;
  uint32_t timestamp;
  uint32_t ping_timestamp;
  uint32_t script_timestamp;
  uint32_t events;

  union
  {
    struct
    {
      uint32_t reset                        : 1;
    };
    uint32_t msk;
  } req;

  const bt_script_t *current_script;
} bt_t;

// ======================================= Declaration =============================================

void bt_rx_process(void);
void bt_tx_script(const bt_script_t *script);

uint8_t cmp_str_with_buf(const char *str, uint8_t *buf, uint16_t buf_size);

bt_t bt;

const bt_rx_event_t bt_rx_events[] =
{
    {.str = "OK",       .msk = BT_EVENT__RX_OK},
    {.str = "SETNAME",  .msk = BT_EVENT__RX_SET_NAME},
    {.str = "SETPIN",   .msk = BT_EVENT__RX_SET_PIN},
    {.str = ""},
};

static const bt_script_t bt_script__ping =
{
    .str = "AT", .timeout = 1000, .rx_ok_msk = BT_EVENT__RX_OK,
};

// ======================================== Implementation =========================================

void bt_init(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  GPIO_InitStructure.GPIO_Pin = BT_RESET_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_Init(BT_RESET_GPIO, &GPIO_InitStructure);

  BT_RESET_UP();

  bt.req.msk = 0;
  START_TIMER(bt.timestamp);
  bt.state = BT_ST__WAIT_BEFORE_START;
}

void bt_process(void)
{
  switch (bt.state)
  {
    case BT_ST__WAIT_BEFORE_START:
      if (CHECK_TIMER(bt.timestamp, 1000))
      {
        bt.state = BT_ST__START;
      }
    break;

    case BT_ST__START:
      BT_RESET_DOWN();
      START_TIMER(bt.timestamp);
      bt.state = BT_ST__RESET_DELAY;
    break;

    case BT_ST__RESET_DELAY:
      if (CHECK_TIMER(bt.timestamp, 500))
      {
        bt.state = BT_ST__PRE_RUNNING;
      }
    break;

    case BT_ST__PRE_RUNNING:
      bt.events = 0;
      bt.rx_process_buf_idx = 0;
      START_TIMER(bt.ping_timestamp);
      bt.state = BT_ST__RUNNING;
    break;

    case BT_ST__RUNNING:
      if (bt.req.reset)
      {
        bt.req.reset = 0;
        bt.state = BT_ST__START;
      }

      if (CHECK_TIMER(bt.ping_timestamp, 2000))
      {
        START_TIMER(bt.ping_timestamp);

        bt_tx_script(&bt_script__ping);
        bt.state = BT_ST__WAIT_SCRIPT_OK;
      }
    break;

    case BT_ST__WAIT_SCRIPT_OK:
      if (CHECK_TIMER(bt.script_timestamp, bt.current_script->timeout))
      {
        bt.state = BT_ST__RUNNING;
        bt.events |= BT_EVENT__TIMEOUT;
        break;
      }

      if ((bt.events & bt.current_script->rx_ok_msk) == bt.current_script->rx_ok_msk)
      {
        bt.state = BT_ST__RUNNING;
        break;
      }
    break;
  }

  bt_rx_process();
}

void bt_tx_script(const bt_script_t *script)
{
  bt.current_script = script;
  bt.events &= ~(bt.current_script->rx_ok_msk | BT_EVENT__TIMEOUT);

  bt_uart_tx_fifo_put_str((char *)bt.current_script->str);

  START_TIMER(bt.script_timestamp);
}

void bt_reset_req(void)
{
  bt.req.reset = 1;
}

void bt_rx_process(void)
{
  uint8_t data;
  while (bt_uart_rx_fifo_get(&data) == ERR_OK)
  {
    // если в буфере уже нет места
    if (bt.rx_process_buf_idx >= BT_RX_PROCESS_BUF_SIZE)
    {
      bt.rx_process_buf_idx = 0;
    }

    data = TO_UP_CASE(data);
    bt.rx_process_buf[bt.rx_process_buf_idx++] = data;

    for (uint8_t i = 0; bt_rx_events[i].str[0] != 0; i++)
    {
      if (cmp_str_with_buf(bt_rx_events[i].str, bt.rx_process_buf, bt.rx_process_buf_idx) == 0)
      {
        bt.events |= bt_rx_events[i].msk;
        bt.rx_process_buf_idx = 0;
        break;
      }
    }
  }
}

/**
 * @brief Сравнить содержимое буфера со строкой.
 * @param str - строка, с которой производится сравнение (должна быть 0-терминирована)
 * @param buf
 * @param buf_size
 * @return Результат сравнения.
 * Если исходный буфер целиком совпадает с исходной строкой, то будет возращено - 0.
 * Иначе, будет возващено - 1.
 */
uint8_t cmp_str_with_buf(const char *str, uint8_t *buf, uint16_t buf_size)
{
  uint8_t res = 1;

  uint16_t idx = 0;
  while (1)
  {
    if (str[idx] != buf[idx])
    {
      res = 1;
      break;
    }

    idx++;

    // если строка закончилась
    if (str[idx] == 0)
    {
      // если "прошли" не по всему буферу для сравнения
      if (idx < buf_size)
      {
        res = 1;
        break;
      }
      else
      {
        res = 0;
        break;
      }
    }
    else // если строка еще не закончилась
    {
      // если "прошли" по всему буферу для сравнения
      if (idx >= buf_size)
      {
        res = 1;
        break;
      }
    }
  }

  return res;
}
/** @} */
