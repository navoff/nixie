/**
 * @file uart.c
 * @brief
 * @author Постнов В.М.
 * @date 17 мар. 2017 г.
 * @addtogroup
 * @{
*/

#include "config.h"

#include "uart.h"

#include "bluetooth_uart.h"

// ======================================= Definition ==============================================

#define BT_UART_BAUDRATE                9600

#define BT_UART_RX_BUF_SIZE             256
#define BT_UART_TX_BUF_SIZE             256

// ======================================= Declaration =============================================

uint8_t bt_uart_rx_buf[BT_UART_RX_BUF_SIZE];

uint8_t bt_uart_tx_buf[BT_UART_TX_BUF_SIZE];

volatile uint8_t uart_tx_started;

// ======================================== Implementation =========================================

void bt_uart_init(void)
{
  // xxx
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
  GPIO_PinRemapConfig(GPIO_Remap_USART1, ENABLE);

  uart_init_struct_t uart_init_struct;
  uart_init_struct.baudrate = BT_UART_BAUDRATE;
  uart_init_struct.uart_number = BT_UART_NUMBER;

  uart_init_struct.tx.mode = UART_LINE_MODE__IRQ;
  uart_init_struct.tx.gpio_idx = BT_UART_TX_GPIO_IDX;
  uart_init_struct.tx.pin_idx = BT_UART_TX_PIN_IDX;
  uart_init_struct.tx.buf = bt_uart_tx_buf;
  uart_init_struct.tx.buf_size = BT_UART_TX_BUF_SIZE;

  uart_init_struct.rx.mode = UART_LINE_MODE__DMA;
  uart_init_struct.rx.gpio_idx = BT_UART_RX_GPIO_IDX;
  uart_init_struct.rx.pin_idx = BT_UART_RX_PIN_IDX;
  uart_init_struct.rx.dma_number = BT_UART_RX_DMA_NUMBER;
  uart_init_struct.rx.dma_channel_idx = BT_UART_RX_DMA_CHANNEL_NUMBER - 1;
  uart_init_struct.rx.buf = bt_uart_rx_buf;
  uart_init_struct.rx.buf_size = BT_UART_RX_BUF_SIZE;

  uart_x_init(BT_UART_NUMBER, &uart_init_struct);

  GPIO_InitTypeDef GPIO_InitStruct;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_InitStruct.GPIO_Pin = GPIO_Pin_5;
  GPIO_Init(GPIOB, &GPIO_InitStruct);
  GPIOB->BSRR = GPIO_Pin_5;
}

error_t bt_uart_rx_fifo_get(uint8_t *data)
{
  return uart_x_rx_byte(BT_UART_NUMBER, data);
}

error_t bt_uart_tx_fifo_put(uint8_t data)
{
  return uart_x_tx_byte(BT_UART_NUMBER, data);
}

error_t bt_uart_tx_fifo_put_str(char *str)
{
  return uart_x_tx_str(BT_UART_NUMBER, str);
}

/** @} */
