/**
 * @file main.c
 * @brief
 * @author Постнов В.М.
 * @date 3 дек. 2016 г.
 * @addtogroup
 * @{
*/

#include "config.h"

#include <string.h>

#include "tick_timer.h"
#include "leds.h"
#include "backleds.h"
#include "backlight.h"
#include "btns.h"
#include "tube.h"
#include "rtc.h"
#include "clock_app.h"
#include "nvram.h"
#include "bluetooth.h"
#include "bluetooth_uart.h"

// ======================================= Definition ==============================================

// ======================================= Declaration =============================================

volatile uint8_t at = 0;
volatile uint8_t at_name1 = 0;
volatile uint8_t at_name2 = 0;
volatile uint8_t at_pin1 = 0;
volatile uint8_t at_pin2 = 0;
volatile uint8_t bt_reset = 0;
  
// ======================================== Implementation =========================================

int main(void)
{
  nvram_settings_init();
  tick_timer_init(NULL);
  leds_init();
  backleds_init();
  rtc_init();
  bt_uart_init();
  tube_init();
  btns_init();
  backlight_init();
  bt_init();
  clock_app_init();

  uint32_t timestamp_1ms;
  START_TIMER(timestamp_1ms);

  uint32_t timestamp_10ms;
  START_TIMER(timestamp_10ms);

  uint32_t timestamp_100ms;
  START_TIMER(timestamp_100ms);

  uint32_t timestamp_1sec;
  START_TIMER(timestamp_1sec);

  uint32_t timestamp_led;

  while(1)
  {
    if (at)
    {
      at = 0;
      bt_uart_tx_fifo_put_str("AT");
    }
    if (at_name1)
    {
      at_name1 = 0;
      bt_uart_tx_fifo_put_str("AT+NAMERetroClock1");
    }
    if (at_name2)
    {
      at_name2 = 0;
      bt_uart_tx_fifo_put_str("AT+NAMERetroClock2");
    }
    if (at_pin1)
    {
      at_pin1 = 0;
      bt_uart_tx_fifo_put_str("AT+PIN1111");
    }
    if (at_pin2)
    {
      at_pin2 = 0;
      bt_uart_tx_fifo_put_str("AT+PIN2222");
    }
    if (bt_reset)
    {
      bt_reset = 0;
      bt_reset_req();
    }

    if (CHECK_TIMER(timestamp_1ms, 1))
    {
      START_TIMER(timestamp_1ms);

      tube_process();
    }

    if (CHECK_TIMER(timestamp_10ms, 10))
    {
      START_TIMER(timestamp_10ms);

      btns_process();
      nvram_settings_process();
    }

    if (CHECK_TIMER(timestamp_100ms, 100))
    {
      START_TIMER(timestamp_100ms);

      rtc_process();
      bt_process();
      clock_app_process();
      backlight_process();
    }

    if (CHECK_TIMER(timestamp_1sec, 1000))
    {
      START_TIMER(timestamp_1sec);

      LED1_ON();
      START_TIMER(timestamp_led);
    }

    if (CHECK_TIMER(timestamp_led, 10))
    {
      START_TIMER(timestamp_led);

      LED1_OFF();
    }
  }
}

/** @} */
