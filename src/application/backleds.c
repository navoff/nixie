/**
 * @file backleds.c
 * @brief
 * @author Постнов В.М.
 * @date 8 мар. 2017 г.
 * @addtogroup
 * @{
*/

#include "config.h"

#include "gpio.h"
#include "pwm.h"

#include "backleds.h"

// ======================================= Definition ==============================================

#if (HW_TYPE == HW_TYPE__RETRO_CLOCK_REV001)

  #error Данный тип платы не поддерживается

#elif (HW_TYPE == HW_TYPE__RETRO_CLOCK_REV002)

  #define PWM_TIMER_ENABLE()  PWM_TIMER->CR1 |= TIM_CR1_CEN
  #define PWM_TIMER_DISABLE() PWM_TIMER->CR1 &= ~TIM_CR1_CEN

  #define SET_HW_CCR0(cmp)    PWM_TIMER->CCR1 = cmp

  #define SET_HW_CCR1(cmp)    PWM_TIMER->CCR2 = cmp

  #define SET_HW_CCR2(cmp)    PWM_TIMER->CCR3 = cmp

  #define SET_HW_CCR3(cmp)    PWM_TIMER->CCR4 = cmp

#else
  #error Не задан тип платы
#endif

/// 10 мкс = 100кГц
#define BACKLEDS_PWM_FREQ             100000

// ======================================= Declaration =============================================

void backleds_timer_init(void);

pwm_t pwm[BACKLED_AMOUNT];

// ======================================== Implementation =========================================

void backled0_on(void)          { gpio_pin_cmd(BACKLED0_GPIO_IDX, BACKLED0_PIN_IDX, ENABLE);  }
void backled0_off(void)         { gpio_pin_cmd(BACKLED0_GPIO_IDX, BACKLED0_PIN_IDX, DISABLE); }
void set_hw_ccr0(uint16_t cmp)  { SET_HW_CCR0(cmp); }

void backled1_on(void)          { gpio_pin_cmd(BACKLED1_GPIO_IDX, BACKLED1_PIN_IDX, ENABLE);  }
void backled1_off(void)         { gpio_pin_cmd(BACKLED1_GPIO_IDX, BACKLED1_PIN_IDX, DISABLE); }
void set_hw_ccr1(uint16_t cmp)  { SET_HW_CCR1(cmp); }

void backled2_on(void)          { gpio_pin_cmd(BACKLED2_GPIO_IDX, BACKLED2_PIN_IDX, ENABLE);  }
void backled2_off(void)         { gpio_pin_cmd(BACKLED2_GPIO_IDX, BACKLED2_PIN_IDX, DISABLE); }
void set_hw_ccr2(uint16_t cmp)  { SET_HW_CCR2(cmp); }

void backled3_on(void)          { gpio_pin_cmd(BACKLED3_GPIO_IDX, BACKLED3_PIN_IDX, ENABLE);  }
void backled3_off(void)         { gpio_pin_cmd(BACKLED3_GPIO_IDX, BACKLED3_PIN_IDX, DISABLE); }
void set_hw_ccr3(uint16_t cmp)  { SET_HW_CCR3(cmp); }

void backleds_init(void)
{
  gpio_rcc_cmd(0, ENABLE);
  gpio_rcc_cmd(1, ENABLE);

  gpio_pin_set_mode(BACKLED0_GPIO_IDX, BACKLED0_PIN_IDX, GPIO_MODE_OUT_PP);
  gpio_pin_set_mode(BACKLED1_GPIO_IDX, BACKLED1_PIN_IDX, GPIO_MODE_OUT_PP);
  gpio_pin_set_mode(BACKLED2_GPIO_IDX, BACKLED2_PIN_IDX, GPIO_MODE_OUT_PP);
  gpio_pin_set_mode(BACKLED3_GPIO_IDX, BACKLED3_PIN_IDX, GPIO_MODE_OUT_PP);

  backled0_off();
  backled1_off();
  backled2_off();
  backled3_off();

  pwm_init(&pwm[0], backled0_on, backled0_off, set_hw_ccr0);
  pwm_init(&pwm[1], backled1_on, backled1_off, set_hw_ccr1);
  pwm_init(&pwm[2], backled2_on, backled2_off, set_hw_ccr2);
  pwm_init(&pwm[3], backled3_on, backled3_off, set_hw_ccr3);

  backleds_timer_init();
}

void backleds_off(uint8_t msk)
{
  for (uint8_t i = 0; i < BACKLED_AMOUNT; i++)
  {
    if (msk & 0x01)
    {
      pwm[i].off_req = 1;
    }

    msk >>= 1;
  }
}

void backleds_on(uint8_t msk)
{
  for (uint8_t i = 0; i < BACKLED_AMOUNT; i++)
  {
    if (msk & 0x01)
    {
      pwm[i].on_req = 1;
    }

    msk >>= 1;
  }
}

void backleds_start_pulse(uint8_t msk)
{
  for (uint8_t i = 0; i < BACKLED_AMOUNT; i++)
  {
    if (msk & 0x01)
    {
      pwm[i].start_pulse_req = 1;
    }

    msk >>= 1;
  }
}

void backleds_set_pwm_inc(int8_t dull_inc, int8_t bright_inc)
{
  for (uint8_t i = 0; i < BACKLED_AMOUNT; i++)
  {
    pwm[i].dull_inc = dull_inc;
    pwm[i].bright_inc = bright_inc;
  }
}

void backleds_timer_init(void)
{
  // настройка таймера
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

  TIM_TimeBaseStructure.TIM_Period = PWM_CNT_MAX;
  TIM_TimeBaseStructure.TIM_Prescaler = (SystemCoreClock / BACKLEDS_PWM_FREQ) - 1;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

  TIM_TimeBaseInit(PWM_TIMER, &TIM_TimeBaseStructure);

  uint16_t TIM_IT = TIM_IT_Update;
  #if (BACKLED_AMOUNT > 0)
  TIM_IT |= TIM_IT_CC1;
  #endif
  #if (BACKLED_AMOUNT > 1)
  TIM_IT |= TIM_IT_CC2;
  #endif
  #if (BACKLED_AMOUNT > 2)
  TIM_IT |= TIM_IT_CC3;
  #endif
  #if (BACKLED_AMOUNT > 3)
  TIM_IT |= TIM_IT_CC4;
  #endif
  #if (BACKLED_AMOUNT > 4)
    #error Добавить обработчик
  #endif
  TIM_ITConfig(PWM_TIMER, TIM_IT, ENABLE);

  PWM_TIMER_ENABLE();
}

#pragma inline=forced
void backleds_process(void)
{
  #if (BACKLED_AMOUNT > 0)
  pwm_process(&pwm[0]);
  #endif

  #if (BACKLED_AMOUNT > 1)
  pwm_process(&pwm[1]);
  #endif

  #if (BACKLED_AMOUNT > 2)
  pwm_process(&pwm[2]);
  #endif

  #if (BACKLED_AMOUNT > 3)
  pwm_process(&pwm[3]);
  #endif

  #if (BACKLED_AMOUNT > 4)
    #error Добавить обработчик
  #endif
}

void TIM2_IRQHandler(void)
{
  PWM_TIMER_DISABLE();

  if (PWM_TIMER->SR & TIM_SR_UIF)
  {
    PWM_TIMER->SR = ~TIM_SR_UIF;

    backleds_process();
  }

  #if (BACKLED_AMOUNT > 0)
  if (PWM_TIMER->SR & TIM_SR_CC1IF)
  {
    PWM_TIMER->SR = ~TIM_SR_CC1IF;

    backled0_off();
  }
  #endif

  #if (BACKLED_AMOUNT > 1)
  if (PWM_TIMER->SR & TIM_SR_CC2IF)
  {
    PWM_TIMER->SR = ~TIM_SR_CC2IF;

    backled1_off();
  }
  #endif

  #if (BACKLED_AMOUNT > 2)
  if (PWM_TIMER->SR & TIM_SR_CC3IF)
  {
    PWM_TIMER->SR = ~TIM_SR_CC3IF;

    backled2_off();
  }
  #endif

  #if (BACKLED_AMOUNT > 3)
  if (PWM_TIMER->SR & TIM_SR_CC4IF)
  {
    PWM_TIMER->SR = ~TIM_SR_CC4IF;

    backled3_off();
  }
  #endif

  #if (BACKLED_AMOUNT > 4)
    #error Добавить обработчик
  #endif

  PWM_TIMER_ENABLE();
}

/** @} */
