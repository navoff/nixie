/**
 * @file backlight.h
 * @brief
 * @author Постнов В.М.
 * @date 12 мар. 2017 г.
 * @addtogroup
 * @{
*/

#ifndef BACKLIGHT_H_
#define BACKLIGHT_H_

// ======================================= Definition ==============================================

#define BACKLIGHT_EFFECT_IDX__DEFAULT     1

// ======================================= Declaration =============================================

void backlight_init(void);
void backlight_process(void);

void backlight_set_next_effect(void);
void backlight_set_effect_off(void);
void backlight_set_effect_reverse(void);

#endif /* BACKLIGHT_H_ */

/** @} */
