/**
 * @file nvram_settings.h
 * @author Постнов В.М.
 * @date 3 окт. 2017 г.
 * @brief Файл содержит структуру с пользовательскими настройками.
 * @addtogroup
 * @{
*/

#ifndef NVRAM_SETTINGS_H_
#define NVRAM_SETTINGS_H_

#include "errors.h"
#include "backlight.h"
#include "tube.h"

// ================================ Definition ================================

/// Версия настроек NVRAM
#define NVRAM_SETTINGS_VERSION          1

#pragma pack(push, 1)

/// Структура с настройками устройства
/// Добавлять новые поля необходимо в конец
/// При добавлении необходимо увеличить версию настроек NVRAM_SETTINGS_VERSION
typedef struct
{
  uint8_t backlight_effect_idx;
  tube_dot_mode_t tube_dot_mode;
} nvram_settings_t;

#pragma pack(pop)

// =============================== Declaration ================================

void nvram_settings_init(void);
void nvram_settings_process(void);
void nvram_settings_set_default(void);

extern const nvram_settings_t nvram_settings_default;

#endif /* NVRAM_SETTINGS_H_ */

/** @} */
