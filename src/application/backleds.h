/**
 * @file backleds.h
 * @brief
 * @author Постнов В.М.
 * @date 8 мар. 2017 г.
 * @addtogroup
 * @{
*/

#ifndef BACKLEDS_H_
#define BACKLEDS_H_

// ======================================= Definition ==============================================

// ======================================= Declaration =============================================

void backleds_init(void);
void backleds_off(uint8_t msk);
void backleds_on(uint8_t msk);
void backleds_start_pulse(uint8_t msk);
void backleds_set_pwm_inc(int8_t dull_inc, int8_t bright_inc);

#endif /* BACKLEDS_H_ */

/** @} */
