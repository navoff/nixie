/**
 * @file clock_app.h
 * @brief
 * @author Постнов В.М.
 * @date 11 мар. 2017 г.
 * @addtogroup
 * @{
*/

#ifndef CLOCK_APP_H_
#define CLOCK_APP_H_

// ======================================= Definition ==============================================

// ======================================= Declaration =============================================

void clock_app_init(void);
void clock_app_process(void);

#endif /* CLOCK_APP_H_ */

/** @} */
