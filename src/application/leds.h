/**
 * @file leds.h
 * @brief
 * @author Постнов В.М.
 * @date 28 янв. 2017 г.
 * @addtogroup
 * @{
*/

#ifndef LEDS_H_
#define LEDS_H_

#include "config.h"

#include "gpio.h"

// ======================================= Definition ==============================================

#define LED1_ON()       gpio_pin_cmd(LED1_GPIO_IDX, LED1_PIN_IDX, ENABLE)
#define LED1_OFF()      gpio_pin_cmd(LED1_GPIO_IDX, LED1_PIN_IDX, DISABLE)
#define LED1_TOGGLE()   gpio_pin_toogle(LED1_GPIO_IDX, LED1_PIN_IDX)

#define LED2_ON()       gpio_pin_cmd(LED2_GPIO_IDX, LED2_PIN_IDX, ENABLE)
#define LED2_OFF()      gpio_pin_cmd(LED2_GPIO_IDX, LED2_PIN_IDX, DISABLE)
#define LED2_TOGGLE()   gpio_pin_toogle(LED2_GPIO_IDX, LED2_PIN_IDX)

// ======================================= Declaration =============================================

void leds_init(void);

#endif /* LEDS_H_ */

/** @} */
