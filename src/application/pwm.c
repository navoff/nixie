/**
 * @file pwm.c
 * @brief
 * @author Постнов В.М.
 * @date 8 мар. 2017 г.
 * @addtogroup
 * @{
*/

#include "config.h"
#include "pwm.h"

// ======================================= Definition ==============================================

// ======================================= Declaration =============================================

// ======================================== Implementation =========================================

void pwm_init(
    pwm_t *pwm,
    void (*led_on)(void),
    void (*led_off)(void),
    void (*set_hw_ccr)(uint16_t cmp))
{
  pwm->led_on = led_on;
  pwm->led_off = led_off;
  pwm->set_hw_ccr = set_hw_ccr;
  pwm->dull_inc = 1;
  pwm->bright_inc = 1;
  pwm->state = PWM_ST__WAIT;
}

void pwm_process(pwm_t *pwm)
{
  if (pwm->on_req)
  {
    pwm->on_req = 0;

    pwm->cmp = PWM_ON_MIN + 1;
    pwm->set_hw_ccr(pwm->cmp);
    pwm->repetition_cnt = 0;
  }

  if (pwm->start_pulse_req)
  {
    pwm->start_pulse_req = 0;

    pwm->state = PWM_ST__ON;
  }

  if (pwm->off_req)
  {
    pwm->off_req = 0;

    pwm->cmp = 0;
    pwm->led_off();
    pwm->state = PWM_ST__WAIT;
  }

  pwm->repetition_cnt++;
  if (pwm->repetition_cnt >= PWM_REPETITION)
  {
    pwm->repetition_cnt = 0;
    int16_t inc = pwm->dull_inc;
    if (pwm->cmp >= PWM_FAST_CNT_THRESHOLD)
    {
      inc = pwm->bright_inc;
    }

    if (pwm->state == PWM_ST__ON)
    {
      pwm->cmp += inc;
      if (pwm->cmp >= PWM_ON_MAX)
      {
        pwm->cmp = PWM_ON_MAX;
        pwm->state = PWM_ST__OFF;
      }
      pwm->set_hw_ccr(pwm->cmp);
    }
    else if (pwm->state == PWM_ST__OFF)
    {
      pwm->cmp -= inc;
      if (pwm->cmp <= PWM_ON_MIN)
      {
        pwm->cmp = PWM_ON_MIN;
        pwm->state = PWM_ST__WAIT;
      }
      pwm->set_hw_ccr(pwm->cmp);
    }
  }

  if (pwm->cmp != 0)
  {
    pwm->led_on();
  }
}

/** @} */
