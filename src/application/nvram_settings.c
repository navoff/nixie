/**
 * @file nvram_settings.c
 * @author Постнов В.М.
 * @date 3 окт. 2017 г.
 * @brief
 * @addtogroup
 * @{
*/

#include "config.h"

#include <stdbool.h>

#include "check_params.h"
#include "nvram.h"

#include "nvram_settings.h"

// ================================ Definition ================================

// =============================== Declaration ================================

const nvram_settings_t nvram_settings_default =
{
    .backlight_effect_idx = BACKLIGHT_EFFECT_IDX__DEFAULT,
    .tube_dot_mode = TUBE_DOT_MODE__BLINK,
};

nvram_t nvram_settings;

// ======================================== Implementation =========================================

void nvram_settings_init(void)
{
  check_params(sizeof(nvram_settings_t) <= NVRAM_USER_SIZE);

  nvram_init_struct_t init_struct = { 0 };
  if (nvram_struct_init(&init_struct, NVRAM_DRIVER__INT_FLASH) != ERR_OK)
  {
    check_params(false);
  }

  nvram_struct_init(&init_struct, NVRAM_DRIVER__INT_FLASH);
  init_struct.version = NVRAM_SETTINGS_VERSION;
  if (nvram_init(&nvram_settings, &init_struct) != ERR_OK)
  {
    nvram_settings_set_default();
  }
}

void nvram_settings_process(void)
{
  nvram_process(&nvram_settings);
}

void nvram_settings_set_default(void)
{
  nvram_write_default_struct(
      &nvram_settings, (uint8_t *) &nvram_settings_default, sizeof(nvram_settings_t));
}

/** @} */
