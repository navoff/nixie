/**
 * @file btns.c
 * @brief
 * @author Постнов В.М.
 * @date 11 мар. 2017 г.
 * @addtogroup
 * @{
*/

#include "config.h"

#include <string.h>

#include "gpio.h"
#include "tick_timer.h"
#include "leds.h"

#include "btns.h"

// ======================================= Definition ==============================================

#define BTN_CHANGE_STATE_IDX_THRESHOLD      10
#define BTN_LONGPRESS_DURATION              1000

typedef struct
{
  uint8_t gpio_idx;
  uint8_t pin_idx;
  uint8_t input_state_idx;
  uint8_t is_pressed;
  uint8_t is_long_press;
  uint8_t idx;
  uint32_t press_timestamp;
} btn_t;

// ======================================= Declaration =============================================

void btn_init(btn_t *btn);
void btn_process(btn_t *btn);

btn_t btns[BTN_AMOUNT];
btn_event_t btn_last_event;

//=============================== Implementation ==============================

void btns_init(void)
{
  btns[0].gpio_idx = BTN1_GPIO_IDX;
  btns[0].pin_idx = BTN1_PIN_IDX;

  btns[1].gpio_idx = BTN2_GPIO_IDX;
  btns[1].pin_idx = BTN2_PIN_IDX;

  for (uint8_t i = 0; i < BTN_AMOUNT; i++)
  {
    btns[i].idx = i;
    btn_init(&btns[i]);
  }

  btn_last_event.type = BTN_EVENT_T__NONE;
}

void btns_process(void)
{
  for (uint8_t i = 0; i < BTN_AMOUNT; i++)
  {
    btn_process(&btns[i]);
  }
}

void btns_get_and_clear_last_event(btn_event_t *last_event)
{
  memcpy(last_event, &btn_last_event, sizeof(btn_event_t));
  btn_last_event.type = BTN_EVENT_T__NONE;
}

void btn_init(btn_t *btn)
{
  gpio_rcc_cmd(btn->gpio_idx, ENABLE);
  gpio_pin_set_in_mode(btn->gpio_idx, btn->pin_idx, GPIO_PIN_PULL__UP);

  btn->input_state_idx = 0;
  btn->is_pressed = 0;
  btn->is_long_press = 0;
}

void btn_process(btn_t *btn)
{
  uint8_t is_pressed = (gpio_pin_state(btn->gpio_idx, btn->pin_idx) == false);

  if (btn->is_pressed == 0)
  {
    if (is_pressed)
    {
      btn->input_state_idx++;
    }
    else
    {
      btn->input_state_idx = 0;
    }

    if (btn->input_state_idx >= BTN_CHANGE_STATE_IDX_THRESHOLD)
    {
      btn->input_state_idx = 0;

      START_TIMER(btn->press_timestamp);
      btn->is_pressed = 1;
    }
  }
  else if (btn->is_pressed)
  {
    if (btn->is_long_press == 0)
    {
      if (CHECK_TIMER(btn->press_timestamp, BTN_LONGPRESS_DURATION))
      {
        btn->is_long_press = 1;

        btn_last_event.btn_idx = btn->idx;
        btn_last_event.type = BTN_EVENT_T__LONGPRESS;
      }
    }

    if (is_pressed == 0)
    {
      btn->input_state_idx++;
    }
    else
    {
      btn->input_state_idx = 0;
    }

    if (btn->input_state_idx >= BTN_CHANGE_STATE_IDX_THRESHOLD)
    {
      btn->input_state_idx = 0;

      if (btn->is_long_press == 0)
      {
        btn_last_event.btn_idx = btn->idx;
        btn_last_event.type = BTN_EVENT_T__CLICK;
      }

      btn->is_pressed = 0;
      btn->is_long_press = 0;
    }
  }
}

/** @} */
