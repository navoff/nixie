/**
 * @file bluetooth.h
 * @brief
 * @author Постнов В.М.
 * @date 23 мар. 2017 г.
 * @addtogroup
 * @{
*/

#ifndef BLUETOOTH_H_
#define BLUETOOTH_H_

// ======================================= Definition ==============================================

// ======================================= Declaration =============================================

void bt_init(void);
void bt_process(void);
void bt_reset_req(void);

#endif /* BLUETOOTH_H_ */

/** @} */
