/**
 * @file uart.h
 * @brief
 * @author Постнов В.М.
 * @date 17 мар. 2017 г.
 * @addtogroup
 * @{
*/

#ifndef UART_H_
#define UART_H_

#include "errors.h"

// ======================================= Definition ==============================================

// ======================================= Declaration =============================================

void bt_uart_init(void);
error_t bt_uart_rx_fifo_get(uint8_t *data);
error_t bt_uart_tx_fifo_put(uint8_t data);
error_t bt_uart_tx_fifo_put_str(char *str);

#endif /* UART_H_ */

/** @} */
