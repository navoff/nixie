/**
 * @file clock_app.c
 * @brief
 * @author Постнов В.М.
 * @date 11 мар. 2017 г.
 * @addtogroup
 * @{
*/

#include "config.h"
#include "btns.h"
#include "rtc.h"
#include "tube.h"
#include "nvram.h"
#include "backlight.h"
#include "clock_app.h"
#include "bluetooth_uart.h"

// ======================================= Definition ==============================================

#define BTN_IDX__MODE         0
#define BTN_IDX__SELECT       1

#define IS_LONGPRESS_SELECT_BTN(btn_event) ((btn_event.type == BTN_EVENT_T__LONGPRESS) && \
                                            (btn_event.btn_idx == BTN_IDX__SELECT))

#define IS_LONGPRESS_MODE_BTN(btn_event)   ((btn_event.type == BTN_EVENT_T__LONGPRESS) && \
                                            (btn_event.btn_idx == BTN_IDX__MODE))

#define IS_CLICK_SELECT_BTN(btn_event)     ((btn_event.type == BTN_EVENT_T__CLICK) && \
                                            (btn_event.btn_idx == BTN_IDX__SELECT))

#define IS_CLICK_MODE_BTN(btn_event)       ((btn_event.type == BTN_EVENT_T__CLICK) && \
                                            (btn_event.btn_idx == BTN_IDX__MODE))

#define IS_CLICK_ANY_BTN(btn_event)        (btn_event.type == BTN_EVENT_T__CLICK)

// ======================================= Declaration =============================================

void clock_app_inc_time_by_place(rtc_time_t *time, uint8_t place_idx);

struct
{
  enum
  {
    CLOCK_APP_ST__IND_TEST,
    CLOCK_APP_ST__DISPLAY_TIME,
    CLOCK_APP_ST__SET_TIME,
  } state;
  rtc_time_t new_time;
  uint8_t new_time_was_changed;
  uint8_t blink_place_idx;
} clock_app;

// ======================================== Implementation =========================================

void clock_app_init(void)
{
  rtc_time_t time;
  rtc_get_current_time(&time);
  tube_display_static_time(&time);

  clock_app.state = CLOCK_APP_ST__DISPLAY_TIME;
}

void clock_app_process(void)
{
  btn_event_t btn_event;
  btns_get_and_clear_last_event(&btn_event);

  switch (clock_app.state)
  {
    case CLOCK_APP_ST__IND_TEST:
      if (IS_CLICK_ANY_BTN(btn_event))
      {
        tube_display_off();
      }

      if (tube_display_is_off())
      {
        rtc_time_t time;
        rtc_get_current_time(&time);
        tube_display_static_time(&time);
        clock_app.state = CLOCK_APP_ST__DISPLAY_TIME;
      }
    break;

    case CLOCK_APP_ST__DISPLAY_TIME:
      if (rtc_check_and_clear_sec_flag())
      {
        rtc_time_t time;
        rtc_get_current_time(&time);
        tube_display_static_time(&time);
      }

      if (IS_LONGPRESS_SELECT_BTN(btn_event))
      {
        backlight_set_next_effect();
      }

      if (IS_LONGPRESS_MODE_BTN(btn_event))
      {
        rtc_get_current_time(&clock_app.new_time);
        clock_app.blink_place_idx = 0;
        clock_app.new_time_was_changed = 0;
        tube_display_blink_time(&clock_app.new_time, clock_app.blink_place_idx);
        clock_app.state = CLOCK_APP_ST__SET_TIME;
        break;
      }
    break;

    case CLOCK_APP_ST__SET_TIME:
      if (IS_CLICK_MODE_BTN(btn_event))
      {
        clock_app.blink_place_idx++;
        if (clock_app.blink_place_idx >= TUBE_BLINK_AMOUNT)
        {
          clock_app.blink_place_idx = 0;
        }
        tube_set_blink_position(clock_app.blink_place_idx);
      }

      if (IS_CLICK_SELECT_BTN(btn_event))
      {
        if (clock_app.blink_place_idx == TUBE_DOT_IDX)
        {
          tube_dot_set_next_mode();
          clock_app.state = CLOCK_APP_ST__DISPLAY_TIME;
          break;
        }
        else
        {
          clock_app.new_time_was_changed = 1;
          clock_app_inc_time_by_place(&clock_app.new_time, clock_app.blink_place_idx);
          tube_display_blink_time(&clock_app.new_time, clock_app.blink_place_idx);
        }
      }

      if (IS_LONGPRESS_SELECT_BTN(btn_event))
      {
        tube_start_display_ind_test();

        clock_app.state = CLOCK_APP_ST__IND_TEST;
        break;
      }

      if (IS_LONGPRESS_MODE_BTN(btn_event))
      {
        if (clock_app.new_time_was_changed)
        {
          clock_app.new_time.second_hi = 0;
          clock_app.new_time.second_lo = 0;
          rtc_set_current_time(&clock_app.new_time);
          tube_display_static_time(&clock_app.new_time);
        }
        clock_app.state = CLOCK_APP_ST__DISPLAY_TIME;
        break;
      }
    break;
  }
}

void clock_app_inc_time_by_place(rtc_time_t *time, uint8_t place_idx)
{
  if (place_idx >= TUBE_AMOUNT) return;

  // младший разряд минут
  if (place_idx == 0)
  {
    time->minute_lo++;
    if (time->minute_lo >= 10)
    {
      time->minute_lo = 0;
    }
  }
  // старший разряд минут
  else if (place_idx == 1)
  {
    time->minute_hi++;
    if (time->minute_hi >= 6)
    {
      time->minute_hi = 0;
    }
  }
  // младший разряд часов
  else if (place_idx == 2)
  {
    time->hour_lo++;
    if ((time->hour_lo >= 4) && (time->hour_hi == 2))
    {
      time->hour_hi = 0;
    }

    if (time->hour_lo >= 10)
    {
      time->hour_lo = 0;
    }
  }
  // старший разряд часов
  else if (place_idx == 3)
  {
    time->hour_hi++;
    if ((time->hour_hi == 2) && (time->hour_lo >= 4))
    {
      time->hour_lo = 0;
    }

    if (time->hour_hi >= 3)
    {
      time->hour_hi = 0;
    }
  }
}

/** @} */
