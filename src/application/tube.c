/**
 * @file tube.c
 * @brief
 * @author Постнов В.М.
 * @date 28 янв. 2017 г.
 * @addtogroup
 * @{
*/

#include "config.h"

#include "check_params.h"
#include "gpio.h"
#include "tick_timer.h"
#include "rtc.h"
#include "nvram.h"

#include "tube.h"

// ======================================= Definition ==============================================

/// Длительность включения одной цифры в режиме тестовой индикации (мс)
#define TUBE_IND_TEST_ON_DURATION     500

/// Длительность включения одной цифры в режиме мигания (мс)
#define TUBE_BLINK_ON_DURATION        450
/// Длительность выключения одной цифры в режиме мигания (мс)
#define TUBE_BLINK_OFF_DURATION       150

/// Длительность включения разделителя в обычном режиме (мс)
#define TUBE_DOT_ON_DURATION          200
/// Длительность выключения разделителя в обычном режиме (мс)
#define TUBE_DOT_OFF_DURATION         800

#define TUBE_DOT_ON()                 gpio_pin_cmd(TUBE_DOT_GPIO_IDX, TUBE_DOT_PIN_IDX, ENABLE)
#define TUBE_DOT_OFF()                gpio_pin_cmd(TUBE_DOT_GPIO_IDX, TUBE_DOT_PIN_IDX, DISABLE)

typedef struct
{
  uint8_t gpio_idx;
  uint8_t pin_idx;
} tube_gpio_out_t;

// ======================================= Declaration =============================================

void tube_gpio_init(void);
void tube_dot_process(void);

void tube_place_on(uint8_t idx);
void tube_place_off(uint8_t idx);
void tube_digit_on(uint8_t idx);
void tube_digit_off(uint8_t idx);

void tube_place_all_off(void);
void tube_digit_all_off(void);

const tube_gpio_out_t tube_anodes[TUBE_AMOUNT] =
{
    { .gpio_idx = TUBE_ANODE1_GPIO_IDX, .pin_idx = TUBE_ANODE1_PIN_IDX },
    { .gpio_idx = TUBE_ANODE2_GPIO_IDX, .pin_idx = TUBE_ANODE2_PIN_IDX },
    { .gpio_idx = TUBE_ANODE3_GPIO_IDX, .pin_idx = TUBE_ANODE3_PIN_IDX },
    { .gpio_idx = TUBE_ANODE4_GPIO_IDX, .pin_idx = TUBE_ANODE4_PIN_IDX },
};

const tube_gpio_out_t tube_cathodes[TUBE_DIGIT_AMOUNT] =
{
    { .gpio_idx = TUBE_CATHODE1_GPIO_IDX,  .pin_idx = TUBE_CATHODE1_PIN_IDX  },
    { .gpio_idx = TUBE_CATHODE2_GPIO_IDX,  .pin_idx = TUBE_CATHODE2_PIN_IDX  },
    { .gpio_idx = TUBE_CATHODE3_GPIO_IDX,  .pin_idx = TUBE_CATHODE3_PIN_IDX  },
    { .gpio_idx = TUBE_CATHODE4_GPIO_IDX,  .pin_idx = TUBE_CATHODE4_PIN_IDX  },
    { .gpio_idx = TUBE_CATHODE5_GPIO_IDX,  .pin_idx = TUBE_CATHODE5_PIN_IDX  },
    { .gpio_idx = TUBE_CATHODE6_GPIO_IDX,  .pin_idx = TUBE_CATHODE6_PIN_IDX  },
    { .gpio_idx = TUBE_CATHODE7_GPIO_IDX,  .pin_idx = TUBE_CATHODE7_PIN_IDX  },
    { .gpio_idx = TUBE_CATHODE8_GPIO_IDX,  .pin_idx = TUBE_CATHODE8_PIN_IDX  },
    { .gpio_idx = TUBE_CATHODE9_GPIO_IDX,  .pin_idx = TUBE_CATHODE9_PIN_IDX  },
    { .gpio_idx = TUBE_CATHODE10_GPIO_IDX, .pin_idx = TUBE_CATHODE10_PIN_IDX },
};

struct
{
  /// Режим работы индикации
  enum
  {
    TUBE_ST__OFF,
    /**
     * Режим тестовой индикации
     * В данном режиме будут по очереди включены все цифры всех ламп.
     * Длительность включения одной цифры: TUBE_IND_TEST_ON_DURATION.
     */
    TUBE_ST__IND_TEST,
    TUBE_ST__STATIC_DIGITS,
    TUBE_ST__BLINK_DIGITS,
  } state;

  /// Массив цифр, отображаемых в режиме часов
  uint8_t display_digit[TUBE_AMOUNT];
  /// Цифра отображаемая в текущий момент
  uint8_t current_display_digit;
  /// Номер разряда, включенного в текущий момент
  uint8_t current_display_place_idx;

  struct
  {
    uint32_t timestamp;
    uint8_t is_on;
    tube_dot_mode_t mode;
  } dot;

  struct
  {
    uint32_t timestamp;
    uint8_t is_on;
    /// Номер разряда, который мигает в текущий момент
    /// @note Применятся только в режиме TUBE_ST__BLINK_DIGITS
    uint8_t place_idx;
  } blink;

  struct
  {
    uint32_t timestamp;
    uint8_t place_idx;
    uint8_t digit;
  } ind_test;

} tube_sm;

// ======================================== Implementation =========================================

void tube_init(void)
{
  tube_gpio_init();

  NVRAM_SETTINGS_READ_FIELD(tube_dot_mode, &tube_sm.dot.mode);

  tube_sm.dot.is_on = 0;
  START_TIMER(tube_sm.dot.timestamp);

  tube_sm.state = TUBE_ST__OFF;
}

void tube_process(void)
{
  tube_dot_process();

  if (tube_sm.state == TUBE_ST__IND_TEST)
  {
    if (CHECK_TIMER(tube_sm.ind_test.timestamp, TUBE_IND_TEST_ON_DURATION))
    {
      START_TIMER(tube_sm.ind_test.timestamp);

      // выключить текущую цифру
      tube_digit_off(tube_sm.ind_test.digit);

      // перейти к следующей цифре
      tube_sm.ind_test.digit++;

      if (tube_sm.ind_test.digit < TUBE_DIGIT_AMOUNT)
      {
        tube_digit_on(tube_sm.ind_test.digit);
      }
      else // если нужно перейти к следующему разряду
      {
        tube_sm.ind_test.digit = 0;

        // выключить текущий разряд
        tube_place_off(tube_sm.ind_test.place_idx);

        // перейти к следующему разряду
        tube_sm.ind_test.place_idx++;

        if (tube_sm.ind_test.place_idx < TUBE_AMOUNT)
        {
          tube_place_on(tube_sm.ind_test.place_idx);
          tube_digit_on(tube_sm.ind_test.digit);
        }
        else
        {
          // тест окончен
          tube_sm.state = TUBE_ST__OFF;
        }
      }
    }
  }
  else if ((tube_sm.state == TUBE_ST__STATIC_DIGITS) ||
           (tube_sm.state == TUBE_ST__BLINK_DIGITS))
  {
    // выключить текущий разряд
    tube_place_off(tube_sm.current_display_place_idx);

    tube_digit_off(tube_sm.current_display_digit);

    // перейти к следующему разряду
    tube_sm.current_display_place_idx++;
    if (tube_sm.current_display_place_idx >= TUBE_AMOUNT)
    {
      tube_sm.current_display_place_idx = 0;
    }

    // выставить нужную цифру на разряде
    // включить следующий разряд
    tube_sm.current_display_digit = tube_sm.display_digit[tube_sm.current_display_place_idx];
    if (tube_sm.state == TUBE_ST__STATIC_DIGITS)
    {
      tube_digit_on(tube_sm.current_display_digit);
      tube_place_on(tube_sm.current_display_place_idx);

    }
    else
    {
      // если это разряд, который в текущий момент мигает
      if (tube_sm.current_display_place_idx == tube_sm.blink.place_idx)
      {
        if (tube_sm.blink.is_on)
        {
          if (CHECK_TIMER(tube_sm.blink.timestamp, TUBE_BLINK_ON_DURATION))
          {
            START_TIMER(tube_sm.blink.timestamp);
            tube_sm.blink.is_on = 0;
          }
        }
        else
        {
          if (CHECK_TIMER(tube_sm.blink.timestamp, TUBE_BLINK_OFF_DURATION))
          {
            START_TIMER(tube_sm.blink.timestamp);
            tube_sm.blink.is_on = 1;
          }
        }

        if (tube_sm.blink.is_on)
        {
          tube_digit_on(tube_sm.current_display_digit);
          tube_place_on(tube_sm.current_display_place_idx);
        }
      }
      else
      {
        tube_digit_on(tube_sm.current_display_digit);
        tube_place_on(tube_sm.current_display_place_idx);
      }
    }
  }
}

void tube_dot_process(void)
{
  if (tube_sm.state == TUBE_ST__IND_TEST)
  {
    if (tube_sm.dot.is_on == 0)
    {
      TUBE_DOT_ON();
      tube_sm.dot.is_on = 1;
    }
  }
  else if (tube_sm.state == TUBE_ST__STATIC_DIGITS)
  {
    if (tube_sm.dot.mode == TUBE_DOT_MODE__BLINK)
    {
      // обработка состояния разделителя
      if (tube_sm.dot.is_on)
      {
        if (CHECK_TIMER(tube_sm.dot.timestamp, TUBE_DOT_ON_DURATION))
        {
          START_TIMER(tube_sm.dot.timestamp);

          TUBE_DOT_OFF();
          tube_sm.dot.is_on = 0;
        }
      }
      else
      {
        if (CHECK_TIMER(tube_sm.dot.timestamp, TUBE_DOT_OFF_DURATION))
        {
          START_TIMER(tube_sm.dot.timestamp);

          TUBE_DOT_ON();
          tube_sm.dot.is_on = 1;
        }
      }
    }
    else if (tube_sm.dot.mode == TUBE_DOT_MODE__OFF)
    {
      if (tube_sm.dot.is_on)
      {
        TUBE_DOT_OFF();
        tube_sm.dot.is_on = 0;
      }
    }
    else if (tube_sm.dot.mode == TUBE_DOT_MODE__ON)
    {
      if (tube_sm.dot.is_on == 0)
      {
        TUBE_DOT_ON();
        tube_sm.dot.is_on = 1;
      }
    }
  }
  else if (tube_sm.state == TUBE_ST__BLINK_DIGITS)
  {
    if (tube_sm.blink.place_idx == TUBE_DOT_IDX)
    {
      // обработка состояния разделителя
      if (tube_sm.dot.is_on)
      {
        if (CHECK_TIMER(tube_sm.dot.timestamp, TUBE_BLINK_ON_DURATION))
        {
          START_TIMER(tube_sm.dot.timestamp);

          TUBE_DOT_OFF();
          tube_sm.dot.is_on = 0;
        }
      }
      else
      {
        if (CHECK_TIMER(tube_sm.dot.timestamp, TUBE_BLINK_OFF_DURATION))
        {
          START_TIMER(tube_sm.dot.timestamp);

          TUBE_DOT_ON();
          tube_sm.dot.is_on = 1;
        }
      }
    }
    else
    {
      if (tube_sm.dot.is_on == 0)
      {
        TUBE_DOT_ON();
        tube_sm.dot.is_on = 1;
      }
    }
  }
}

/**
 * Запустить тестовый режим работы индикации (см TUBE_ST__IND_TEST).
 */
void tube_start_display_ind_test(void)
{
  tube_display_clear();

  tube_sm.ind_test.digit = 0;
  tube_sm.ind_test.place_idx = 0;

  tube_place_on(tube_sm.ind_test.place_idx);
  tube_digit_on(tube_sm.ind_test.digit);

  START_TIMER(tube_sm.ind_test.timestamp);
  tube_sm.state = TUBE_ST__IND_TEST;
}

void tube_from_time_to_digits(uint8_t *digits, rtc_time_t *time)
{
  digits[0] = time->minute_lo;
  digits[1] = time->minute_hi;
  digits[2] = time->hour_lo;
  digits[3] = time->hour_hi;
}

void tube_dot_set_next_mode(void)
{
  tube_sm.dot.mode++;
  if (tube_sm.dot.mode >= TUBE_DOT_MODE__AMOUNT)
  {
    tube_sm.dot.mode = (tube_dot_mode_t)0;
  }

  NVRAM_SETTINGS_WRITE_FIELD(tube_dot_mode, &tube_sm.dot.mode);
}

void tube_display_static_time(rtc_time_t *time)
{
  tube_from_time_to_digits(tube_sm.display_digit, time);
  tube_sm.state = TUBE_ST__STATIC_DIGITS;
}

void tube_display_blink_time(rtc_time_t *time, uint8_t place_idx)
{
  if (place_idx >= TUBE_AMOUNT) return;

  tube_from_time_to_digits(tube_sm.display_digit, time);

  tube_sm.blink.place_idx = place_idx;

  if (tube_sm.state != TUBE_ST__BLINK_DIGITS)
  {
    tube_display_clear();
    tube_sm.blink.is_on = 0;
    START_TIMER(tube_sm.ind_test.timestamp);
  }

  tube_sm.state = TUBE_ST__BLINK_DIGITS;
}

void tube_set_blink_position(uint8_t place_idx)
{
  if (place_idx >= TUBE_BLINK_AMOUNT) return;
  if (tube_sm.state != TUBE_ST__BLINK_DIGITS) return;

  tube_sm.blink.place_idx = place_idx;
}

void tube_display_off()
{
  tube_display_clear();
  tube_sm.state = TUBE_ST__OFF;
}

uint8_t tube_display_is_off(void)
{
  return (tube_sm.state == TUBE_ST__OFF);
}

void tube_gpio_init(void)
{
  gpio_rcc_cmd(0, ENABLE);
  gpio_rcc_cmd(1, ENABLE);

  #if (HW_TYPE == HW_TYPE__RETRO_CLOCK_REV001)

  #elif (HW_TYPE == HW_TYPE__RETRO_CLOCK_REV002)

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
  GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);

  #else
    #error Не задан тип платы
  #endif

  for (uint8_t i = 0; i < TUBE_AMOUNT; i++)
  {
    gpio_pin_set_mode(tube_anodes[i].gpio_idx, tube_anodes[i].pin_idx, GPIO_MODE_OUT_PP);
    gpio_pin_cmd(tube_anodes[i].gpio_idx, tube_anodes[i].pin_idx, DISABLE);
  }

  for (uint8_t i = 0; i < TUBE_DIGIT_AMOUNT; i++)
  {
    gpio_pin_set_mode(tube_cathodes[i].gpio_idx, tube_cathodes[i].pin_idx, GPIO_MODE_OUT_PP);
    gpio_pin_cmd(tube_cathodes[i].gpio_idx, tube_cathodes[i].pin_idx, DISABLE);
  }

  gpio_pin_set_mode(TUBE_DOT_GPIO_IDX, TUBE_DOT_PIN_IDX, GPIO_MODE_OUT_PP);
  TUBE_DOT_OFF();
}

/**
 * @brief Включить один разряд (одну лампу) индикации.
 * @note Остальные разряды при этом остаются в предыдущем состоянии.
 * @param idx - номер раздяда. Может принимать значение от 0 до TUBE_AMOUNT
 */
void tube_place_on(uint8_t idx)
{
  check_params(idx < TUBE_AMOUNT);

  gpio_pin_cmd(tube_anodes[idx].gpio_idx, tube_anodes[idx].pin_idx, ENABLE);
}

/**
 * @brief Выключить один разряд (одну лампу) индикации.
 * @note Остальные разряды при этом остаются в предыдущем состоянии.
 * @param idx - номер раздяда. Может принимать значение от 0 до (TUBE_AMOUNT - 1)
 */
void tube_place_off(uint8_t idx)
{
  check_params(idx < TUBE_AMOUNT);

  gpio_pin_cmd(tube_anodes[idx].gpio_idx, tube_anodes[idx].pin_idx, DISABLE);
}

/**
 * @brief Включить одну цифру (знак на лампе).
 * @note Остальные цифры при этом остаются в предыдущем состоянии.
 * @param idx - номер цифры. Может принимать значение от 0 до (TUBE_DIGIT_AMOUNT - 1)
 */
void tube_digit_on(uint8_t idx)
{
  check_params(idx < TUBE_DIGIT_AMOUNT);

  gpio_pin_cmd(tube_cathodes[idx].gpio_idx, tube_cathodes[idx].pin_idx, ENABLE);
}

/**
 * @brief Выключить одну цифру (знак на лампе).
 * @note Остальные цифры при этом остаются в предыдущем состоянии.
 * @param idx - номер цифры. Может принимать значение от 0 до (TUBE_DIGIT_AMOUNT - 1)
 */
void tube_digit_off(uint8_t idx)
{
  check_params(idx < TUBE_DIGIT_AMOUNT);

  gpio_pin_cmd(tube_cathodes[idx].gpio_idx, tube_cathodes[idx].pin_idx, DISABLE);
}

/**
 * @brief Выключить все разряды (лампы) индикации.
 */
void tube_place_all_off(void)
{
  for (uint8_t i = 0; i < TUBE_AMOUNT; i++)
  {
    tube_place_off(i);
  }
}

/**
 * @brief Выключить все цифры (знаки на лампе).
 */
void tube_digit_all_off(void)
{
  for (uint8_t i = 0; i < TUBE_DIGIT_AMOUNT; i++)
  {
    tube_digit_off(i);
  }
}

void tube_display_clear(void)
{
  tube_place_all_off();
  tube_digit_all_off();
  TUBE_DOT_OFF();
  tube_sm.dot.is_on = 0;
}

/** @} */
