/**
 * @file tube.h
 * @brief
 * @author Постнов В.М.
 * @date 28 янв. 2017 г.
 * @addtogroup
 * @{
*/

#ifndef TUBE_H_
#define TUBE_H_

#include "rtc.h"

// ======================================= Definition ==============================================

#define TUBE_AMOUNT             4
#define TUBE_DIGIT_AMOUNT       10

#define TUBE_BLINK_AMOUNT       (TUBE_AMOUNT + 1)
#define TUBE_DOT_IDX            TUBE_AMOUNT

typedef enum
{
  TUBE_DOT_MODE__BLINK  = 0,
  TUBE_DOT_MODE__ON     = 1,
  TUBE_DOT_MODE__OFF    = 2,
} tube_dot_mode_t;
#define TUBE_DOT_MODE__AMOUNT       3

// ======================================= Declaration =============================================

void tube_init(void);
void tube_process(void);

void tube_display_clear(void);
void tube_start_display_ind_test(void);
void tube_display_static_time(rtc_time_t *time);
void tube_display_blink_time(rtc_time_t *time, uint8_t place_idx);
void tube_dot_set_next_mode(void);
void tube_set_blink_position(uint8_t place_idx);
void tube_display_off(void);
uint8_t tube_display_is_off(void);

#endif /* TUBE_H_ */

/** @} */
