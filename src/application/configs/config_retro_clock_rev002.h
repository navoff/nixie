/**
 * @file config_retro_clock_rev002.h
 * @author Постнов В.М.
 * @date 9 мая 2018 г.
 * @brief
 * @addtogroup
 * @{
*/

#ifndef CONFIG_RETRO_CLOCK_REV002_H_
#define CONFIG_RETRO_CLOCK_REV002_H_

// ================================ Definition ================================

/**
 * @brief Модификации плат
 * @addtogroup DEVICE_HW__MODIFICATIONS
 * @{
 */
  /// @note Базовая модификация без изменений
  #define HW_MOD_0                              0
/** @} */

/// Выбранная модификация платы
#define HW_MODIFICATION                         HW_MOD_0

/// Смещение таблицы прерываний
#define VECT_TAB_OFFSET                         0x00000


/// Будет ли в проект добавлена работа с NVRAM на встроенной flash
#define FEATURE_ENABLE_INT_NVRAM                1

/// Размер NVRAM памяти.
/// Не должен превышать размера одного семента внутренней flash
/// Должен быть кратен 1 кБайту
#define NVRAM_SIZE                              256

#define LED1_GPIO_IDX                           0
#define LED1_PIN_IDX                            3

#define LED2_GPIO_IDX                           0
#define LED2_PIN_IDX                            4

#define BTN_AMOUNT                              2

#define BTN1_GPIO_IDX                           0
#define BTN1_PIN_IDX                            2

#define BTN2_GPIO_IDX                           0
#define BTN2_PIN_IDX                            1

#define TUBE_ANODE1_GPIO_IDX                    0
#define TUBE_ANODE1_PIN_IDX                     12

#define TUBE_ANODE2_GPIO_IDX                    0
#define TUBE_ANODE2_PIN_IDX                     9

#define TUBE_ANODE3_GPIO_IDX                    1
#define TUBE_ANODE3_PIN_IDX                     0

#define TUBE_ANODE4_GPIO_IDX                    0
#define TUBE_ANODE4_PIN_IDX                     6


#define TUBE_CATHODE1_GPIO_IDX                  1
#define TUBE_CATHODE1_PIN_IDX                   3

#define TUBE_CATHODE2_GPIO_IDX                  0
#define TUBE_CATHODE2_PIN_IDX                   15

#define TUBE_CATHODE3_GPIO_IDX                  1
#define TUBE_CATHODE3_PIN_IDX                   10

#define TUBE_CATHODE4_GPIO_IDX                  1
#define TUBE_CATHODE4_PIN_IDX                   11

#define TUBE_CATHODE5_GPIO_IDX                  1
#define TUBE_CATHODE5_PIN_IDX                   12

#define TUBE_CATHODE6_GPIO_IDX                  1
#define TUBE_CATHODE6_PIN_IDX                   13

#define TUBE_CATHODE7_GPIO_IDX                  1
#define TUBE_CATHODE7_PIN_IDX                   14

#define TUBE_CATHODE8_GPIO_IDX                  1
#define TUBE_CATHODE8_PIN_IDX                   15

#define TUBE_CATHODE9_GPIO_IDX                  0
#define TUBE_CATHODE9_PIN_IDX                   8

#define TUBE_CATHODE10_GPIO_IDX                 1
#define TUBE_CATHODE10_PIN_IDX                  4


#define TUBE_DOT_GPIO_IDX                       1
#define TUBE_DOT_PIN_IDX                        2


#define BACKLED_AMOUNT                          4

#define BACKLED0_GPIO_IDX                       0
#define BACKLED0_PIN_IDX                        11

#define BACKLED1_GPIO_IDX                       0
#define BACKLED1_PIN_IDX                        10

#define BACKLED2_GPIO_IDX                       1
#define BACKLED2_PIN_IDX                        1

#define BACKLED3_GPIO_IDX                       0
#define BACKLED3_PIN_IDX                        7

#define PWM_TIMER                               TIM2


#define BT_UART_NUMBER                          1

#define BT_UART_RX_GPIO_IDX                     1
#define BT_UART_RX_PIN_IDX                      7

#define BT_UART_TX_GPIO_IDX                     1
#define BT_UART_TX_PIN_IDX                      6

#define BT_UART_RX_DMA_NUMBER                   1
#define BT_UART_RX_DMA_CHANNEL_NUMBER           5
#define BT_UART_TX_DMA_CHANNEL_NUMBER           4


#define FEATURE_ENABLE_UART1                    1
#define FEATURE_ENABLE_UART2                    0
#define FEATURE_ENABLE_UART3                    0
#define FEATURE_ENABLE_UART4                    0
#define FEATURE_ENABLE_UART5                    0
#define FEATURE_ENABLE_UART6                    0
#define FEATURE_ENABLE_UART7                    0
#define FEATURE_ENABLE_UART8                    0

// =============================== Declaration ================================


#endif /* CONFIG_RETRO_CLOCK_REV002_H_ */

/** @} */
