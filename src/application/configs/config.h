/**
 * @file config.h
 * @brief
 * @author Постнов В.М.
 * @date 27 янв. 2017 г.
 * @addtogroup
 * @{
*/

#ifndef CONFIG_H_
#define CONFIG_H_

// ======================================= Definition ==============================================

/**
 * @defgroup HW_TYPE_LIST
 * Типы (идентификаторы) плат.
 * @{
 */
  /// Первый вариант платы rev001
  #define HW_TYPE__RETRO_CLOCK_REV001             0

  /// Второй вариант платы rev002
  #define HW_TYPE__RETRO_CLOCK_REV002             1
/** @} */

/**
 * @defgroup FW_TYPE_LIST
 * Тип (идентификаторы) прошивок
 * @{
 */
  /// Загрузчик
  #define FW_TYPE__DEFAULT                        0x00000001
/** @} */

// Тип платы выбирать здесь:
//#define HW_TYPE                                   HW_TYPE__RETRO_CLOCK_REV001
#define HW_TYPE                                   HW_TYPE__RETRO_CLOCK_REV002

// Тип прошивки выбирать здесь:
#define FW_TYPE                                   FW_TYPE__DEFAULT


#if defined(DEBUG)
  #define USE_CHECK_PARAMS_EN                 1
#elif defined(RELEASE)
  #define USE_CHECK_PARAMS_EN                 0
#else
  #error В настройках проекта нужно указать тип сборки для данной цели компиляции!
#endif

/**
 * @brief Версия прошивки
 * @addtogroup FW_VERSIONS
 * @{
 */
  /// Версия прошивки
  #define DEVICE_INFO__FW_VERSION_MAJOR       1

  /// Подверсия прошивки
  #define DEVICE_INFO__FW_VERSION_MINOR       0

  /// Версия сборки
  #define DEVICE_INFO__FW_BUILD               0x1
/** @} */

#if (HW_TYPE == HW_TYPE__RETRO_CLOCK_REV001)
  #include "config_retro_clock_rev001.h"
  #include "stm32f10x.h"
  #define STM32F1XX

#elif (HW_TYPE == HW_TYPE__RETRO_CLOCK_REV002)
  #include "config_retro_clock_rev002.h"
  #include "stm32f10x.h"
  #define STM32F1XX

#endif

#include "hal_config.h"

// ======================================= Declaration =============================================

#endif /* CONFIG_H_ */

/** @} */
