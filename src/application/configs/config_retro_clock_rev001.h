/**
 * @file config_retro_clock_rev001.h
 * @author Постнов В.М.
 * @date 9 мая 2018 г.
 * @brief
 * @addtogroup
 * @{
*/

#ifndef CONFIG_RETRO_CLOCK_REV001_H_
#define CONFIG_RETRO_CLOCK_REV001_H_

// ================================ Definition ================================

/// Будет ли в проект добавлена работа с NVRAM на встроенной flash
#define FEATURE_ENABLE_INT_NVRAM                1

/// Размер NVRAM памяти.
/// Не должен превышать размера одного семента внутренней flash
/// Должен быть кратен 1 кБайту
#define NVRAM_SIZE                              256

#define TUBE_ANODE1_GPIO_IDX                    1
#define TUBE_ANODE1_PIN_IDX                     12

#define TUBE_ANODE2_GPIO_IDX                    1
#define TUBE_ANODE2_PIN_IDX                     15

#define TUBE_ANODE3_GPIO_IDX                    0
#define TUBE_ANODE3_PIN_IDX                     11

#define TUBE_ANODE4_GPIO_IDX                    0
#define TUBE_ANODE4_PIN_IDX                     12

#define TUBE_CATHODE1_GPIO_IDX                  1
#define TUBE_CATHODE1_PIN_IDX                   8

#define TUBE_CATHODE2_GPIO_IDX                  1
#define TUBE_CATHODE2_PIN_IDX                   7

#define TUBE_CATHODE3_GPIO_IDX                  0
#define TUBE_CATHODE3_PIN_IDX                   5

#define TUBE_CATHODE4_GPIO_IDX                  0
#define TUBE_CATHODE4_PIN_IDX                   4

#define TUBE_CATHODE5_GPIO_IDX                  0
#define TUBE_CATHODE5_PIN_IDX                   3

#define TUBE_CATHODE6_GPIO_IDX                  0
#define TUBE_CATHODE6_PIN_IDX                   6

#define TUBE_CATHODE7_GPIO_IDX                  0
#define TUBE_CATHODE7_PIN_IDX                   7

#define TUBE_CATHODE8_GPIO_IDX                  1
#define TUBE_CATHODE8_PIN_IDX                   0

#define TUBE_CATHODE9_GPIO_IDX                  1
#define TUBE_CATHODE9_PIN_IDX                   1

#define TUBE_CATHODE10_GPIO_IDX                 1
#define TUBE_CATHODE10_PIN_IDX                  9

#define FEATURE_ENABLE_UART1                    1
#define FEATURE_ENABLE_UART2                    0
#define FEATURE_ENABLE_UART3                    0
#define FEATURE_ENABLE_UART4                    0
#define FEATURE_ENABLE_UART5                    0
#define FEATURE_ENABLE_UART6                    0
#define FEATURE_ENABLE_UART7                    0
#define FEATURE_ENABLE_UART8                    0

// =============================== Declaration ================================


#endif /* CONFIG_RETRO_CLOCK_REV001_H_ */

/** @} */
